import React from 'react'
import PreApp from './PreApp'

import {Provider } from 'react-redux';

import createStore from './store/store';
const store = createStore()



export default App = () => {
    return (
        <Provider store = {store}>
            <PreApp/>
        </Provider>
    )
}