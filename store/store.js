import { createStore, applyMiddleware } from 'redux';
import logger from 'redux-logger';
import thunk from 'redux-thunk'
import {composeWithDevTools} from 'redux-devtools-extension'

import rootReducer from '../src/reducers'
import {fetchPosts} from '../src/actions/images'

export default () => {
    const store = createStore(rootReducer, composeWithDevTools(applyMiddleware(logger, thunk)))
    store.dispatch(fetchPosts('reactjs')).then(() => console.log(store.getState()))
    return store;
    
}

