import React from 'react'
import { StyleSheet, TouchableOpacity, Image, Dimension } from 'react-native'
import { useNavigation } from '@react-navigation/native'
 

const AppNavigator = ({ uri, fullImg }) => {

  const { navigate } = useNavigation()
  return (
    <TouchableOpacity
      onPress={() => navigate('FullImg', { uri: fullImg })}>
      <Image
        style={styles.image}
        source={{ uri }} />
    </TouchableOpacity>
  )
}

const styles = StyleSheet.create({
  image: {
    maxHeight:150,
        minHeight:60,
        width:150
  }
})

export default AppNavigator