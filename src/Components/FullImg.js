import React from 'react';
import { StyleSheet, View, Image,Dimensions} from 'react-native';
import { useRoute } from '@react-navigation/native'

const FullImg = () => {
    const { params: { uri } } = useRoute()
    return(
        <View style={styles.container}>
            <Image style={styles.img}
                        source={{
                            uri
                        }}
            />
        </View>
    )
    }
export default FullImg

const styles = StyleSheet.create({
    container:{
    },
    img:{
        minHeight: Dimensions.get('window').height,
        width:Dimensions.get('window').width,
        resizeMode: 'stretch'
    }
});
    