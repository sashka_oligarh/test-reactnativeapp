import React, { Component } from 'react';
import { 
    StyleSheet, 
    Text, 
    View,  
    FlatList, 
    Dimensions,
 } 
    from 'react-native';
import {connect} from 'react-redux';

import AppNavigator from './AppNavigator'

const mapStateToProps = state => {
    return {
        images: state.images.items
    }
}

class Galery extends Component { 
    
    render(){
        console.log(this.props.images)
        const {images} = this.props
        return(
            <View style={styles.kek}>
                <FlatList style={styles.container}
                    data={images}
                    renderItem={({item}) => 
                    <View>
                        <View key={item.id} style={styles.card}>
                            <Text style={styles.text}>{item.alt_description}</Text>
                            <AppNavigator uri={item.urls.thumb} fullImg={item.urls.regular}/>
                            <Text style={styles.text}>{item.user.name}</Text>
                        </View>
                    </View>}
                    numColumns={2}
                />
            </View>
        )
    }
}

export default connect(mapStateToProps)(Galery)

const styles = StyleSheet.create({
    kek:{
        
        justifyContent:'center',
        alignItems:'center',
        alignContent:'center',
        height: Dimensions.get('window').height,
        width: Dimensions.get('window').width, 
    },
    container:{
        marginVertical: 30,
        height: Dimensions.get('window').height,
        width: Dimensions.get('window').width,   
    },
    card:{
        minHeight:100,
        minWidth: Dimensions.get('window').width*0.45, 
        minHeight: Dimensions.get('window').height*0.3,
        borderWidth: 2,
        borderColor:'#000',
        marginHorizontal:10,
        marginVertical:10,
        justifyContent:'center',
        alignItems:'center',
        padding:0,
    },
    img:{
        maxHeight:150,
        minHeight:60,
        width:150
    },
    text:{
        maxWidth:150,
        marginVertical:5
    },
});
    