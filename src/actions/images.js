export const setClothes = loading => ({
    type: 'SET_IMAGES',
    payload: loading
})

export const setFail = bool => {
    return {
        type: 'SET_FAIL',
        payload: bool
    };
}

export const setReady = json => {
    return {
        type: 'SET_IS_READY',
        payload: json
    };
}

export function fetchPosts() {
    return function (dispatch) {
      dispatch(setClothes(true))
      return fetch(`https://api.unsplash.com/photos/?client_id=ab3411e4ac868c2646c0ed488dfd919ef612b04c264f3374c97fff98ed253dc9`,
      {method: 'GET',}
      )
        .then(response => response.json(),)
        .then(json =>dispatch(setReady(json)))
        .catch(error =>  setFail(error))
    }
  }

