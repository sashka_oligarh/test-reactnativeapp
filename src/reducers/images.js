const initialState = {
    isLoading: false,
    items: null,
    err:null
    
}
export default (state = initialState, action) => {
    switch (action.type) {
        case 'SET_IMAGES':
            return{
            ...state,
            isLoading: true,
            }
        case 'SET_IS_READY':
            return{
                ...state,
                items:action.payload,
                isLoading: false
            }
        case 'SET_FAIL':
            return{
                ...state,
                isLoading:false,
                err:action.payload
            }
        default:
            return state;
    }
}