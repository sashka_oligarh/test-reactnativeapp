import { StatusBar } from 'expo-status-bar';
import React, { Component } from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux'
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { View, ActivityIndicator, StyleSheet  } from 'react-native';

import Galery from './src/Components/Galery'
import FullImg from './src/Components/FullImg'
import {fetchPosts} from './src/actions/images'



const Stack = createStackNavigator();


const mapStateToProps = state => {
  return {
    loading: state.images.isLoading
  }
}



class PreApp extends Component {


  componentDidMount(){
    fetchPosts()
  }
  render(){

  if (this.props.loading){
    return (
      <View style={styles.loading} >
        <ActivityIndicator />
      </View>
    )
    } else {
    return (
        <NavigationContainer>
          <StatusBar style="auto" />
        <Stack.Navigator >
            <Stack.Screen name="Galery" component={Galery} />
            <Stack.Screen name="FullImg" component={FullImg} />
        </Stack.Navigator>
        </NavigationContainer>
    );
    }
  }
}

const styles = StyleSheet.create({
  loading: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  }
})

export default connect(mapStateToProps)(PreApp)

